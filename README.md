# smb-webdav

#### 介绍
手搓一个低功耗的nas,基于orangePi 3B的个人使用的smb和webdav共享,开通Ddns,可远程访问。orangePi还安装了Kodi通过客厅电视机可观影和看直播电视。
主要的技术：运用docker设置samba和webdav共享。docker基于ubuntu，开启systemd，安装samba服务，webdav基于nginx，编译安装并添加webdav插件。

## 第一步 制作ubuntu镜像
1.拉取ubuntu docker 镜像 
```shell
docker pull ubuntu:latest
```
2.克隆仓库 
```shell 
git clone https://gitee.com/cjh92815/smb-webdav.git 
```
3.构造镜像
```shell 
cd /smb-webdav/ubuntudocker 
docker build -t ubuntu:systemd .
```
注意：这里的 . 很重要，命令中少了这个.就无法构建。<br />
4.运行 ubuntu docker 
```shell
docker run -itd --name ubuntu  --privileged=true --restart always   -p 139:139 -p 445:445 -p 88:80 -v  {宿主机共享目录}:/mount -d ubuntu:systemd
```
注意：本文webdav 端口号：88 samba端口号139，445 如被占用可改一下 <br />
     {宿主机共享目录} ：这个根据实际情况填写 <br />

## 第二步 设置Samba共享
1.进入docker 
```shell 
docker exec -it ubuntu /bin/bash 
```
2.添加用户组shares(shares组名，根据需要可改 6000组编码可改） 
```shell 
groupadd shares -g  6000 
```
3.添加用户test(test用户名，根据需要可改 6000用户编码可改）
```shell
useradd  test -u 6000 -g 6000 -s /sbin/nologin -d /dev/null
```
4.#设置访问密码 
```shell 
smbpasswd -a test
``` 
设置你的samba用户访问密码并记下，访问时要用。<br />
5.配置samba文件
```shell
nano /etc/samba/smb.conf
```
```JSON
[share]
comment = share folder
guest ok = yes
browseable = yes
path = /mount
create mask = 0755
directory mask = 0755
valid users = test
available = yes
writable = yes
```
将上面内容复制到配置文件的最末尾，并保存退出 <br />
5.设置共享文件夹的用户和权限，重启samba服务
```shell
shown -R test:shares /mount
chmod -R 777 /mount
systemctl restart smbd
```



